const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Producto = sequelize.define('Producto', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    price: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    Description:{
        type: DataTypes.STRING,
        allowNull:false
    },

    the_amount: {
        type: DataTypes.STRING,
        allowNull: false
    },
    evaluation: {
        type: DataTypes.STRING,
        allowNull: false
    },
    photograph:{
        type: DataTypes.TEXT,
        allowNull:false
    }
});

Producto.associate = function(models) {
    Producto.belongsTo(models.User);
    Producto.belongsTo(models.Store);
};


module.exports = Producto;